# Why does EditText not show autocomplete/autocorrect suggestions on some devices?

## General

This mini sample project displays a problem on certain devices where Android's
`inputType` XML attribute doesn't show the soft keyboard autocompletion
suggestions on some devices. This was asked as [StackOverflow question
46421849](https://stackoverflow.com/questions/46421849/why-does-edittext-not-show-autocomplete-autocorrect-suggestions-on-some-devices).

It seems that the `autoText` attribute, while being deprecated, is the only way
to work on some old Androids without upgrade paths.

You can [download and test the latest
APK](https://gitlab.com/gradha/Stackoverflow46421849/tags) attached to the most
recent tag, which shows different `EditText` widgets to test if autocomplete
suggestions appear. In theory all fields should show the autocomplete
suggestions but on some devices only the deprecated `autoText` widget works as
intended.
