package es.elhaso.gradha.stackoverflow46421849

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        user_input_edit3.inputType = InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE
    }

    fun onButton(v: View) {
        val url = "https://stackoverflow.com/questions/46421849/"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}
